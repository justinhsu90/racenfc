package nfc.jxdeveloping.com.racenfc;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by justinhsu on 30/05/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "record";
    public static final String DB_ID = "_id";
    public static final String DB_TAG_ID = "TAG_ID";
    public static final String DB_RECORD_TIME = "RECORD_TIME";
    public static final String DB_OLD_MESSAGE = "OLD_MESSAGE";
    public static final String DB_NEW_MESSAGE = "NEW_MESSAGE";
    public static final String DB_RECORD_STATUS = "RECORD_STATUS";
    private static final String DB_FILE = "RecordDB.db";
    private static final int DB_VERSION = 1;


    public DBHelper(Context context) {
        super(context, DBHelper.DB_FILE, null, DBHelper.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    public void createTable(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (");
        sql.append(DB_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
        sql.append("," + DB_TAG_ID + " TEXT");
        sql.append("," + DB_RECORD_TIME + " INTEGER");
        sql.append("," + DB_OLD_MESSAGE + " TEXT");
        sql.append("," + DB_NEW_MESSAGE + " TEXT");
        sql.append("," + DB_RECORD_STATUS + " INTEGER");
        sql.append(");");
        db.execSQL(sql.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        final String SQL = "DROP TABLE " + TABLE_NAME;
        db.execSQL(SQL);
    }
}