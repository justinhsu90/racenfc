package nfc.jxdeveloping.com.racenfc;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Locale;

import nfc.jxdeveloping.com.racenfc.record.TagHolder;


public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private CharSequence mTitle;
    private NewNFCListener newNFCListener;
    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private PendingIntent mPendingIntent;
    private NfcAdapter mAdapter;
    private NdefMessage mNdefPushMessage;
    private RaceDataFragment mRaceDataFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTitle = getTitle();
        mAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        mNdefPushMessage = new NdefMessage(new NdefRecord[]{newTextRecord(
                "Message from NFC Reader :-)", Locale.ENGLISH, true)});
        showFragment(0);
    }

    private NdefRecord newTextRecord(String text, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));

        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = text.getBytes(utfEncoding);

        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);

        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = "MAIN Page";
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    private void resolveIntent(Intent intent) throws IOException, FormatException {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            triggerNewUIForNewNFC();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        System.out.println("get nfc in onNewIntent");
        setIntent(intent);
        try {
            resolveIntent(intent);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FormatException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
            mAdapter.enableForegroundNdefPush(this, mNdefPushMessage);
        }
    }

    private void triggerNewUIForNewNFC() throws IOException, FormatException {
        if (newNFCListener == null) {
            Log.w("Test", "newNFCListener == null");
//            getSupportFragmentManager().beginTransaction().add(fragment, "NFCFragment").commit();
//            newNFCListener = mNFCFragment;
            newNFCListener = NFCFragment.getInstance();

//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    try {
                        newNFCListener.onNewNFC(getIntent());
//                    } catch (Exception e) {
//                        ;
//                    }
//                }
//            }, 100);
        }


    }

    private void initialDB() {
        if (dbHelper == null) {
            dbHelper = new DBHelper(this);
        }
    }


    private void getReadableDB() {
        initialDB();
        db = dbHelper.getReadableDatabase();

    }

    private void getWritableDB() {
        initialDB();
        db = dbHelper.getWritableDatabase();

    }

    private String getTimeFormat(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
        return sdf.format(time);
    }

    private String getDateFormat(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(time);
    }

    private Cursor getAllData() {
        return readDB();
    }

    public Cursor readDB() {
        getReadableDB();
        createTable();
        return db.rawQuery(buildQueryPath(""), null);

    }


    private String structEmailFormat(Cursor cursor) {
        StringBuilder builder = new StringBuilder();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String Id = cursor.getString(1);
            long time = cursor.getLong(2);
            String oldM = cursor.getString(3);
            String newM = cursor.getString(4);
            long status = cursor.getLong(5);
            builder.append("Tag ID : " + Id);
            builder.append("___");
            builder.append("Time : " + time);
            builder.append("___");
            builder.append("Old Message : " + oldM);
            builder.append("___");
            builder.append("New Message : " + newM);
            builder.append("___");
            builder.append("Status : " + status + "\n");
            builder.append("<br/><br/>");
        }
        return builder.toString();
    }

    public String buildQueryPath(String itemID) {
        String SELECT = "select ";
        String ALL = " * ";
        String allColumns = "rowid " + DBHelper.DB_ID + "," + DBHelper.DB_TAG_ID + "," + DBHelper.DB_RECORD_TIME + "," + DBHelper.DB_OLD_MESSAGE + "," + DBHelper.DB_NEW_MESSAGE + "," + DBHelper.DB_RECORD_STATUS;

        String FROM = " from " + DBHelper.TABLE_NAME;
        String output = SELECT + allColumns + FROM;
        return output;
    }

    public void DeleteTable() {
        getWritableDB();
        db.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_NAME);
    }

    public void createTable() {
        dbHelper.createTable(db);
    }


    public boolean storeTagToDB(TagHolder holder) throws JSONException {
        boolean isSucceed = false;
        getWritableDB();
        createTable();
        SQLiteStatement sqLiteStatement = db
                .compileStatement("insert into "
                        + DBHelper.TABLE_NAME
                        + " (" + DBHelper.DB_TAG_ID + "," + DBHelper.DB_RECORD_TIME + "," + DBHelper.DB_OLD_MESSAGE + "," + DBHelper.DB_NEW_MESSAGE + "," + DBHelper.DB_RECORD_STATUS + ") values(?,?,?,?,?)");

        db.beginTransaction();
        try {
            sqLiteStatement.bindString(1, holder.getTagId());
            sqLiteStatement.bindLong(2, holder.getRecordTime());
            sqLiteStatement.bindString(3, holder.getOldMessage());
            sqLiteStatement.bindString(4, holder.getNewMessage());
            sqLiteStatement.bindLong(5, holder.getRecordStatus());
            sqLiteStatement.executeInsert();
            db.setTransactionSuccessful();
            db.endTransaction();
            isSucceed = true;
        } catch (Exception e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }
        sqLiteStatement.close();
        return isSucceed;

    }

    public void sendEmail() {
        Cursor cursor = getAllData();
        if (cursor != null && cursor.moveToNext()) {
            Intent it = new Intent(Intent.ACTION_SEND);
            it.putExtra(Intent.EXTRA_SUBJECT, getDateFormat(System.currentTimeMillis()));
            it.putExtra(Intent.EXTRA_TEXT, structEmailFormat(cursor));
            it.setType("text/plain");
            startActivity(Intent.createChooser(it, "Choose Email Client"));
        } else {
            Toast.makeText(this, " It's nothing to send", Toast.LENGTH_SHORT).show();
        }
    }

    public void showFragment(int toShow) {
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        switch (toShow) {
            case 0:
                ft.addToBackStack(MainFragment.TAG);
                ft.replace(R.id.container, MainFragment.newInstance()).commit();
                break;
            case 1:
//                if (mNFCFragment == null) {
//                    mNFCFragment = NFCFragment.getInstance();
//                }
                ft.addToBackStack(NFCFragment.TAG);
                ft.replace(R.id.container, NFCFragment.getInstance())
                        .commit();
                break;
            case 2:
                ft.addToBackStack(RaceDataFragment.TAG);
                ft.replace(R.id.container, RaceDataFragment.newInstance())
                        .commit();
                break;
            default:
                break;

        }
    }

    private void showConfirmEraseDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Erase Data");
        dialog.setMessage("Do you want ot remove all the records in database ?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeleteTable();

            }
        });
        dialog.setNegativeButton("No", null);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.data_fragment:
                showFragment(2);
                break;
            case R.id.erace_data_button:
                showConfirmEraseDialog();
                break;
            case R.id.scan_fragment:
                showFragment(1);
                break;
            case R.id.send_mail_button:
                sendEmail();
                break;
        }
    }

    public interface NewNFCListener {
        void onNewNFC(Intent intent) throws IOException, FormatException;
    }

}


