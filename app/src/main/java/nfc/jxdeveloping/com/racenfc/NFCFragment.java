package nfc.jxdeveloping.com.racenfc;

import android.app.Activity;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by justinhsu on 30/05/15.
 */
public class NFCFragment extends BaseFragment implements MainActivity.NewNFCListener {
    public final static String TAG = "NFCFragment";

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private TextView mTagId;
    private TextView mTagMessageFromTag;
    private TextView mTagRecordStatus;
    private TextView mTagMessageToStore;
    private TextView mRecordTime;
    private View rootView;
    private static NFCFragment fragment;

    public NFCFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static NFCFragment getInstance() {
        if (fragment == null) {
            fragment = new NFCFragment();
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("get nfc in onCreateView");
        rootView = inflater.inflate(R.layout.fragment_scan_nfc, container, false);
        initialView();
        return rootView;
    }

    private void initialView() {
        System.out.println("get nfc view in initialview");

        if (rootView == null) {
            System.out.println("get nfc view is null");
        }
        mTagId = (TextView) rootView.findViewById(R.id.tag_id_dec_value);
        mTagId.setText("test1234");
        mTagMessageFromTag = (TextView) rootView.findViewById(R.id.tag_stored_data_value);
        mTagRecordStatus = (TextView) rootView.findViewById(R.id.tag_record_status_value);
        mTagMessageToStore = (TextView) rootView.findViewById(R.id.tag_new_data_value);
        mRecordTime = (TextView) rootView.findViewById(R.id.tag_record_time_value);
    }

    private void resolveIntent(Intent intent) throws IOException, FormatException {
        String action = intent.getAction();
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
//            if (rawMsgs != null && rawMsgs.length > 0) {
//                NdefMessage msg = (NdefMessage) rawMsgs[0];
//                System.out.println(("get nfc msg :" + msg));
//                updateUi(getTagId(tag), getTagMessage(msg), "newData", "na", System.currentTimeMillis());
        updateUi();
//            }
//        }
    }

    private String getTimeFormat(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return sdf.format(time);
    }

    //    private void updateUi(String tagid, String storedData, String newDate, String recordStatus, long time) {
    private void updateUi() {
        mTagId.setText("hahah");
        mTagMessageFromTag.setText("hahah");
        mTagRecordStatus.setText("hahah");
        mTagMessageToStore.setText("hahah");
    }


    private String[] getTagTechList(Tag tag) {
        if (tag == null) {
            return null;
        }
        return tag.getTechList();
    }

    private String getTagId(Tag tag) {
        byte[] extraID = tag.getId();
        StringBuilder sb = new StringBuilder();
        for (byte b : extraID) {
            sb.append(String.format("%02X", b));
        }

        String tagID = sb.toString();
        Log.e("nfc ID", tagID);
        System.out.println("get nfc nfc id : " + tagID);
        return tagID;
    }

    private String getTagMessage(NdefMessage msg) {
        try {
            byte[] payload = msg.getRecords()[0].getPayload();
            //Get the Text Encoding
            String textEncoding = ((payload[0] & 0200) == 0) ? "UTF-8" : "UTF-16";

            //Get the Language Code
            int languageCodeLength = payload[0] & 0077;
            String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            System.out.println(("get nfc languageCode :" + languageCode));

            //Get the Text
            String text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
            System.out.println(("get nfc PatientId :" + text));
            return text;

        } catch (Exception e) {
            throw new RuntimeException("Record Parsing Failure!!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("get nfc : onResume");

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        System.out.println("get nfc in onAttach");
    }

    @Override
    public void onNewNFC(Intent intent) throws IOException, FormatException {
        System.out.println("get nfc in onNewNFC");
        initialView();
        mTagId.setText("hahahahahahahahahahah");
        resolveIntent(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("get nfc : on pause");
    }

    @Override
    public void onStop() {
        super.onStop();
        System.out.println("get nfc : onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        System.out.println("get nfc : onDestroyView");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("get nfc : onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        System.out.println("get nfc : onDetach");

    }
}


