package nfc.jxdeveloping.com.racenfc.record;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by justinhsu on 14/06/15.
 */
public class ApplicationEx extends Application {
    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);

    }

}
