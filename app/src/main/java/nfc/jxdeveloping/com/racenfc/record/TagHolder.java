package nfc.jxdeveloping.com.racenfc.record;

/**
 * Created by justinhsu on 06/06/15.
 */
public class TagHolder {
    String tagId;
    String oldMessage;
    String newMessage;
    int recordStatus;
    int recordTime;


    public TagHolder(String tagId, int recordTime, String oldMessage, String newMessage, int recordStatus) {
        this.tagId = tagId;
        this.oldMessage = oldMessage;
        this.newMessage = newMessage;
        this.recordStatus = recordStatus;
        this.recordTime = recordTime;

    }


    public String getTagId() {
        return tagId;
    }

    public int getRecordStatus() {
        return recordStatus;
    }

    public String getNewMessage() {
        return newMessage;
    }

    public String getOldMessage() {
        return oldMessage;
    }

    public int getRecordTime() {
        return recordTime;
    }

}
