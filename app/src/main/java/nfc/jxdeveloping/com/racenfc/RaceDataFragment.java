package nfc.jxdeveloping.com.racenfc;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONException;

import nfc.jxdeveloping.com.racenfc.record.TagHolder;

/**
 * Created by justinhsu on 30/05/15.
 */
public class RaceDataFragment extends BaseFragment {
    public final static String TAG = "RaceDataFragment";

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ListCursorAdapter listCursortAdapter;
    private ListView listview;

    public RaceDataFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static RaceDataFragment newInstance() {
        RaceDataFragment fragment = new RaceDataFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_scanned_data, container, false);
        listview = (ListView) rootView.findViewById(R.id.listview);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    private void runDummy() throws JSONException {

        for (int i =0 ;i <20 ;i++) {
            TagHolder holder = new TagHolder(String.valueOf(i*1000),i*1,String.valueOf(i*2),String.valueOf(i*3),i*4);
            ((MainActivity) getActivity()).storeTagToDB(holder);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            runDummy();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Cursor cursor = ((MainActivity) getActivity()).readDB();
        if (cursor != null && cursor.moveToFirst()) {
            System.out.println("cursor is : "+cursor.getCount());
            listCursortAdapter = new ListCursorAdapter(getActivity(), cursor, 0);
            listview.setAdapter(listCursortAdapter);
        }
    }
}

