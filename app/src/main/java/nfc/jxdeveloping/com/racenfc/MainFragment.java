package nfc.jxdeveloping.com.racenfc;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by justinhsu on 30/05/15.
 */
public class MainFragment extends BaseFragment {
    public final static String TAG = "MainFragment";
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private TextView mScanButton;
    private TextView mDataButton;
    private TextView mEraseButton;
    private TextView mSendDataButton;

    public MainFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        final MainActivity activity = ((MainActivity) getActivity());
        mScanButton = (TextView) rootView.findViewById(R.id.scan_fragment);
        mScanButton.setOnClickListener(activity);
        mDataButton = (TextView) rootView.findViewById(R.id.data_fragment);
        mDataButton.setOnClickListener(activity);

        mEraseButton = (TextView) rootView.findViewById(R.id.erace_data_button);
        mEraseButton.setOnClickListener(activity);

        mSendDataButton = (TextView) rootView.findViewById(R.id.send_mail_button);
        mSendDataButton.setOnClickListener(activity);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
