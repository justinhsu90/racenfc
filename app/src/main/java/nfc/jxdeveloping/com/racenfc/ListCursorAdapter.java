package nfc.jxdeveloping.com.racenfc;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class ListCursorAdapter extends CursorAdapter {
    LayoutInflater inflater;

    public ListCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        // TODO Auto-generated constructor stub
        inflater = LayoutInflater.from(context);
    }

    private String getTimeFormat(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
        return sdf.format(time);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // TODO Auto-generated method stub

        // for listview
        RelativeLayout body = (RelativeLayout) view.findViewById(R.id.list_item_body);
        TextView tagId = (TextView) view.findViewById(R.id.list_item_tag_id);
        TextView recordTime = (TextView) view.findViewById(R.id.list_item_time);
        TextView oldMessage = (TextView) view.findViewById(R.id.list_item_old);
        TextView newMessage = (TextView) view.findViewById(R.id.list_item_new);
        TextView recordStatus = (TextView) view.findViewById(R.id.list_item_status);

        String Id = cursor.getString(1);
        long time = cursor.getLong(2);
        String oldM = cursor.getString(3);
        String newM = cursor.getString(4);
        long status = cursor.getLong(5);

        if (status > 0) {
            recordStatus.setText("Saved");
            body.setBackgroundColor(Color.GREEN);
        } else if (status < 0) {
            recordStatus.setText("Error");
            body.setBackgroundColor(Color.MAGENTA);
        } else {
            recordStatus.setText("NA");
            body.setBackgroundColor(Color.TRANSPARENT);
        }
        tagId.setText(Id);
        recordTime.setText(getTimeFormat(time));
        oldMessage.setText(oldM);
        newMessage.setText(newM);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.list_item, parent, false);
    }

}
